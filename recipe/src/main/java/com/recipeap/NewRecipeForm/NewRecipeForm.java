package com.recipeap.NewRecipeForm;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NewRecipeForm {

     public static void display() {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("New Recipe");

        // Create form elements
        Label nameLabel = new Label("Recipe Name:");
        TextField nameInput = new TextField();

        Label ingredientsLabel = new Label("Ingredients:");
        TextArea ingredientsInput = new TextArea();

        Label instructionsLabel = new Label("Instructions:");
        TextArea instructionsInput = new TextArea();

        Button saveButton = new Button("Save");
        saveButton.setOnAction(e -> {
            // Save the new recipe
            // For now, just print to console
            System.out.println("Recipe Name: " + nameInput.getText());
            System.out.println("Ingredients: " + ingredientsInput.getText());
            System.out.println("Instructions: " + instructionsInput.getText());
            window.close();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(e -> window.close());

        // Layout
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(8);
        gridPane.setHgap(10);

        gridPane.add(nameLabel, 0, 0);
        gridPane.add(nameInput, 1, 0);
        gridPane.add(ingredientsLabel, 0, 1);
        gridPane.add(ingredientsInput, 1, 1);
        gridPane.add(instructionsLabel, 0, 2);
        gridPane.add(instructionsInput, 1, 2);
        gridPane.add(saveButton, 0, 3);
        gridPane.add(cancelButton, 1, 3);

        Scene scene = new Scene(gridPane, 400, 300);
        window.setScene(scene);
        window.showAndWait();
    }
}


