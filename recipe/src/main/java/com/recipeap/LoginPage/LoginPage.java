package com.recipeap.LoginPage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class LoginPage extends Application {

    @Override
    public void start(Stage Loginpg) throws Exception {
        
        Loginpg.setTitle("Recipe Application");
        Loginpg.setWidth(1000);
        Loginpg.setHeight(900);
       // Loginpg.getIcons().add(new Image("recipe/src/main/resources/iconrcp.png"));
         Label usernameLabel = new Label("Username:");
        TextField usernameField = new TextField();

        Label passwordLabel = new Label("Password:");
        PasswordField passwordField = new PasswordField();

         Button loginButton = new Button("Login");
        loginButton.setOnAction(event -> {
            String username = usernameField.getText();
            String password = passwordField.getText();
            
            // Handle login logic here
            if (authenticate(username, password)) {
                System.out.println("Login successful!");
            } else {
                System.out.println("Login failed!");
            }
        });

        // Create a GridPane to hold the form
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        gridPane.add(usernameLabel, 0, 0);
        gridPane.add(usernameField, 1, 0);
        gridPane.add(passwordLabel, 0, 1);
        gridPane.add(passwordField, 1, 1);
        gridPane.add(loginButton, 1, 2);

        Scene scene = new Scene(gridPane, 300, 200);

        Loginpg.setScene(scene);

        Loginpg.show();
    }

    private boolean authenticate(String username, String password) {
        return "user".equals(username) && "pass".equals(password);
        
    }
    
}
